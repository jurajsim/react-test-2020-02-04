import React, { useEffect, useState } from "react";
import List from "./tags-list/list";
import AddField from "./tags-list/add-field";
import styles from "./tags-list.module.scss";
import {
  addTagAndUpdateUrlHash,
  parseUrlHash,
  setTagsAndUpdateUrlHash
} from "../util/url";

export default function TagsList() {
  const [tags, setTags] = useState(parseUrlHash(window.location.hash));

  useEffect(() => {
    function onHashChanged() {
      const parsed = parseUrlHash(window.location.hash);
      //yea I'm not dealing with duplicates here...
      setTags(parsed);
    }

    window.addEventListener("hashchange", onHashChanged, false);
    return function() {
      window.removeEventListener("hashchange", onHashChanged);
    };
  });

  function addTag(tagName) {
    addTagAndUpdateUrlHash(tags, tagName);
  }

  function removeTag(idx) {
    let updatedTags = tags.slice();
    updatedTags.splice(idx, 1);
    setTagsAndUpdateUrlHash(updatedTags);
  }

  return (
    <div className={styles.tagsList}>
      <List tags={tags} onRemove={removeTag}></List>
      <AddField onAdd={addTag} />
    </div>
  );
}
