import React from "react";
import PropTypes from "prop-types";
import styles from "./list.module.scss";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Item from "./list-item";

export default function List({ tags, onRemove }) {
  return (
    <ul className={styles.list}>
      <TransitionGroup className="items-section__list">
        {tags.map((tag, idx) => {
          return (
            <CSSTransition key={`item_${tag}`} timeout={250} classNames="fade">
              <Item onRemove={onRemove} idx={idx} tag={tag}></Item>
            </CSSTransition>
          );
        })}
      </TransitionGroup>
    </ul>
  );
}

List.propTypes = {
  tags: PropTypes.array,
  onRemove: PropTypes.func
};
