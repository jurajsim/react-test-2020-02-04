import React from "react";
import styles from "./list-item.module.scss";

export default function Item({ onRemove, idx, tag }) {
  return (
    <li
      title={`${tag} (Click to remove)`}
      className={styles.listItem}
      onClick={() => onRemove(idx)}
    >
      <div>{decodeURIComponent(tag)}</div>
    </li>
  );
}
