import React, { useState } from "react";
import PropTypes from "prop-types";
import styles from "./add-field.module.scss";
import { tagExists } from "../../util/url";
import classNames from "classnames";

export default function AddField({ onAdd }) {
  const tagInput = React.createRef();

  function addTag() {
    const value = tagInput.current.value;
    tagInput.current.value = "";
    validate();
    onAdd(encodeURIComponent(value));
  }

  function onKeyDown(e) {
    if (!valid) {
      return;
    }
    if (e.key === ",") {
      e.preventDefault();
      e.stopPropagation();
      return;
    }
    if (e.key === "Enter") {
      addTag();
    }
  }

  const [valid, setValid] = useState(false);
  const [error, setError] = useState(null);

  function validate() {
    setError(null);
    if (!tagInput.current.value) {
      setValid(false);
      return;
    }
    if (tagExists(tagInput.current.value)) {
      setError("Tag already exists!");
      setValid(false);
      return;
    }
    setValid(true);
  }

  return (
    <div className={styles.addField}>
      <div className={styles.flex}>
        <input
          className={classNames({
            [styles.input]: true,
            [styles.inputError]: error
          })}
          ref={tagInput}
          onChange={validate}
          onKeyDown={onKeyDown}
        />
        {error ? <div className={styles.error}>* {error}</div> : ""}
        <div className={styles.spacer}></div>
        <button className={styles.button} disabled={!valid} onClick={addTag}>
          Add
        </button>
      </div>
    </div>
  );
}

AddField.propTyles = {
  onAdd: PropTypes.func
};
