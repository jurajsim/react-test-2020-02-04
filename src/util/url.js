export function parseUrlHash(hash) {
  let regex = /#tags=(.*)/;
  let match = regex.exec(hash);
  if (match && match[1]) {
    return match[1].split(",");
  }
  return [];
}

export function tagExists(tagName) {
  const tags = parseUrlHash(window.location.hash);
  return tags.includes(tagName);
}

export function setTagsAndUpdateUrlHash(tags) {
  window.location.hash = `#tags=${tags.join(",")}`;
}

export function addTagAndUpdateUrlHash(existingTags, tagName) {
  const updatedTags = existingTags.slice();
  updatedTags.push(tagName);
  setTagsAndUpdateUrlHash(updatedTags);
}
