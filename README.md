This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# public prod build 

Live: https://rse-test-jurasjim.herokuapp.com/#tags=red,blue,purple
(heroku)

# how to run

install deps first
```bash
npm i
```

## local dev mode
using npm (dev mode) 
```bash
npm start
```

## local prod build
using docker (prod build)
```bash
docker build -t test-app-image 
docker run -e PORT=80 -p 9091:80 test-app-image
```
then open http://localhost:9091

# notes

I took me ~2hrs (which is slow - I know that...) but it was a couple of months since I used react/hooks/create-react-app etc. (I was working with nodejs on backend and vuejs on front instead)

